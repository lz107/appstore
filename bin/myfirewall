#!/bin/bash


# Flush all current rules from iptables
iptables -F
iptables -X
ip6tables -F
ip6tables -X
iptables -t nat -F

# Allow SSH connections on tcp port 22
# This is essential when working on remote servers via SSH to prevent locking yourself out of the system
iptables -A INPUT -p tcp --dport 22 -j ACCEPT
ip6tables -A INPUT -p tcp --dport 22 -j ACCEPT

# Set default policies
iptables -P INPUT DROP
ip6tables -P INPUT DROP
iptables -P FORWARD DROP
ip6tables -P FORWARD DROP
iptables -P OUTPUT ACCEPT
ip6tables -P OUTPUT DROP

# Set access for localhost
iptables -A INPUT -i lo -j ACCEPT
ip6tables -A INPUT -i lo -j ACCEPT

# Accept packets belonging to established and related connections
iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
ip6tables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT

# Allow IPv6 ICMP packets
ip6tables -A INPUT -p icmpv6 -j ACCEPT

# Allow pings
iptables -A INPUT -p icmp --icmp-type echo-request -j ACCEPT
iptables -A OUTPUT -p icmp --icmp-type echo-reply -j ACCEPT
ip6tables -A INPUT -p ipv6-icmp --icmpv6-type echo-request -j ACCEPT
ip6tables -A OUTPUT -p ipv6-icmp --icmpv6-type echo-reply -j ACCEPT

# HTTP, HTTPS
iptables -A INPUT -p tcp --dport 80 -m conntrack --ctstate NEW -j ACCEPT
iptables -A INPUT -p tcp --dport 443 -m conntrack --ctstate NEW -j ACCEPT

# Open 444 for staging access
iptables -A INPUT -p tcp --dport 444 -m conntrack --ctstate NEW -j ACCEPT

# (IPv4-only) Allow access to MySQL and Redis from Mac worker
ALLOW_IP_ADDRESS=152.3.124.200

# MySQL
MYSQL_PORT=3306
# create a new chain
iptables -N mysql-protection
# allow your IP
iptables -A mysql-protection --src $ALLOW_IP_ADDRESS -j ACCEPT
# drop everyone else
iptables -A mysql-protection -j DROP
# use chain xxx for packets coming to TCP port $MYSQL_PORT
iptables -A INPUT -m tcp -p tcp --dport $MYSQL_PORT -j mysql-protection

# Redis
REDIS_PORT=6379
# create a new chain
iptables -N redis-protection
# allow your IP
iptables -A redis-protection --src $ALLOW_IP_ADDRESS -j ACCEPT
# drop everyone else
iptables -A redis-protection -j DROP
# use chain xxx for packets coming to TCP port $REDIS_PORT
iptables -A INPUT -m tcp -p tcp --dport $REDIS_PORT -j redis-protection

# Save settings
iptables-save > /etc/iptables/rules.v4
ip6tables-save > /etc/iptables/rules.v6
systemctl enable netfilter-persistent
systemctl start netfilter-persistent

# List rules
echo "filter table"
echo "============"

echo "IPv4"
echo "----"
iptables -L -v

echo
echo "IPv6"
echo "----"
ip6tables -L -v

echo
echo "nat table (IPv4 only)"
echo "====================="
iptables -t nat -L -v

