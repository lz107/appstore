class RenameAppFilesDownloadCount < ActiveRecord::Migration
  def change
    rename_column :app_files, :downloadCount, :download_count
  end
end
