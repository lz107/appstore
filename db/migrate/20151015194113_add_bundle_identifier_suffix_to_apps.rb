class AddBundleIdentifierSuffixToApps < ActiveRecord::Migration
  def change
    add_column :apps, :bundle_identifier_suffix, :string
  end
end
