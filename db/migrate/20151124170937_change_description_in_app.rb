class ChangeDescriptionInApp < ActiveRecord::Migration
  def change
    change_column :apps, :description, :text
  end
end
