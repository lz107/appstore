json.array!(@authors) do |author|
  json.extract! author, :id, :netid
  json.url author_url(author, format: :json)
end
