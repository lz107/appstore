class AppFilePolicy < ApplicationPolicy
  attr_reader :user, :app_file

  def initialize(user, app_file)
    @user = user
    @app_file = app_file
  end

  def index?
    true
  end

  def show?
    true
  end

  def create?
    app_file.app.user == user || user.try(:admin?)
  end

  def new?
    create?
  end

  def update?
    app_file.app.user == user || user.try(:admin?)
  end

  def edit?
    update?
  end

  def destroy?
    app_file.app.user == user || user.try(:admin?)
  end

end
