class AppPolicy < ApplicationPolicy
  attr_reader :user, :app

  def initialize(user, app)
    @user = user
    @app = app
  end

  def index?
    true
  end

  def show?
    user
  end

  def create?
    user
  end

  def new?
    create?
  end

  def update?
    app.user == user || user.try(:admin?)
  end

  def edit?
    update? || user.try(:admin?)
  end

  def destroy?
    app.user == user || user.try(:admin?)
  end

end
