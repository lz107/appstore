require 'open-uri'

class BuildIpaJob < ActiveJob::Base
  queue_as :mac_online

  def perform(app_file_id)
    ActiveRecord::Base.clear_active_connections!

    # get corresponding app_file record
    app_file = AppFile.find(app_file_id)

    # this job doesn't sign non-iOS apps
    unless app_file.is_ios_app?
      return
    end

    log = ""
    log << "App file code signing status is '#{app_file.code_signing_status}'\n"
    if app_file.unsigned? || app_file.signing_failed?
      app_file.signing!

      tmpdir = Dir.mktmpdir
      success = false
      begin
        log << "#{self.class.name} using tmp dir: #{tmpdir}\n"

        # fetch app file from remote web server (assuming we are worker on another machine)
        # we use remote's HTTP send_file here to reduce ruby workload
        File.open(File.join(tmpdir, 'Unsigned.app.zip'), "wb") do |file|
          url_options = Rails.application.config.action_mailer.default_url_options
          url_options[:format] = 'zip'
          url_options[:download_token] = app_file.download_token
          download_url = Rails.application.routes.url_helpers.app_app_file_download_url(app_file.app, app_file, url_options)
          log << "Downloading from #{download_url}\n"
          open(download_url, "rb", {ssl_verify_mode: OpenSSL::SSL::VERIFY_NONE}) do |read_file|
            file.write(read_file.read)
          end
        end

        # Mac automatically zips .app folder, so we need to unzip it
        log << "Unzipping archive...\n"
        IO.popen(['unzip', File.join(tmpdir, 'Unsigned.app.zip'), '-d', File.join(tmpdir, 'UnsignedArchiveContent')]) do |unzip_io|
          log << unzip_io.read
        end

        # extract unzipped ???.app/ folder name, because people might have renamed the zip archive
        app_folder = nil
        IO.popen(['unzip', '-Z', '-1', File.join(tmpdir, 'Unsigned.app.zip')]) do |unzip_z_1_io|
          first_line = unzip_z_1_io.readlines[0].chomp
          log << "First item in archive: '#{first_line}'\n"
          unless first_line.end_with? ".app/"
            raise "first item in zip archive does not end with .app/"
          end
          app_folder = first_line.chomp('/')
        end

        # make entitlements file
        entitlements = {
          'application-identifier' => 'U6J58C8CJT.' + app_file.app.ios_application_identifier,
          'com.apple.developer.team-identifier' => 'U6J58C8CJT',
          'get-task-allow' => false,
          'keychain-access-groups' => ['U6J58C8CJT.' + app_file.app.ios_application_identifier]
        }
        File.write(File.join(tmpdir, 'entitlements.plist'), entitlements.to_plist)

        log << "Using custom entitlements:\n"
        log << "--- BEGIN ENTITLEMENTS ---\n"
        log << entitlements.to_plist
        log << "--- END ENTITLEMENTS ---\n"

        # call signing script, collect output
        app_file.signed_ipa_path_on_signing_server = File.join(tmpdir, 'Signed.ipa')
        signing_script_args = [
          'floatsign.sh',
          File.join(tmpdir, 'UnsignedArchiveContent', app_folder),  # app archive to sign
          'iPhone Distribution: Duke University',  # cert name in Keychain
          '-e', File.join(tmpdir, 'entitlements.plist'),
          # TODO: allow for multiple provisioning profiles and App IDs
          '-p', Rails.root.join('resources', 'ios_provisioning_profiles', 'CoLab_Member_Apps_Nov_2017.mobileprovision').to_s,
          '-b', app_file.app.ios_application_identifier,
          '-t', File.join(tmpdir, 'floatsign_tmp'),
          app_file.signed_ipa_path_on_signing_server
        ]

        log << "Signing with args: " << signing_script_args.collect { |a| "\"#{a}\"" }.join(' ') << "\n"
        IO.popen(signing_script_args, :err=>[:child, :out]) do |signing_script_io|
          log << signing_script_io.read
        end

        # check exit status
        if $?.to_i != 0
          raise "Signing script exited with non-zero status (#{$?})"
        end

        app_file.pending_transfer_to_web!
        success = true
      rescue => e
        app_file.signing_failed!
        log << "Fatal error: " << e.to_s << "\n"
      end

    else
      log << "Refusing to codesign when app_file is not freshly uploaded\n"
    end

    # save log and states to database without validation and callbacks
    app_file.update_columns(
      code_signing_log: log,
      signed_ipa_path_on_signing_server: app_file.signed_ipa_path_on_signing_server)

    if success
      FetchedSignedIpaJob.perform_later app_file.id
    else
      FileUtils.remove_entry tmpdir
    end
  end
end
