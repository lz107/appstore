## Setup
* Install `iPhone Distribution: Duke University` certificate and private key to `System` keychain on signing server.
    - Get Info > Access Control > Allow all applications to access this item
* Install [Jiehan's fork of floatsign.sh](https://github.com/jiehanzheng/floatsign) and make it part of the PATH
* Install ImageMagick
* Install Redis

## Deploy and maintenance
### Deploy from branch `deploy`
On development machine, deploy to remote:
```
cap production deploy
```

### Auto-start worker on signing server
1. Git clone this repository
2. Gather config files.  Most files should be identical to production web app server, except that the MySQL username is `'appstore_sign'@'152.3.124.119'`, which allows remote usage but with fewer privileges.
```
scp config/database.yml config/secrets.yml config/settings.local.yml appstore@appstore-mac.colab.managed.jiehan.org:~/appstore_sign/config/
# ALSO copy provisioning profile to signing server
# be sure to edit config/database.yml on signing server to use the remote MySQL user
```
3. Set up cron job:
```
@reboot cd /Users/appstore/appstore_sign; bash -l -c 'bundle exec god -c config/god/mac_worker.rb'
```
4. Step 3 simply doesn't work.  So delete the cron job that you just created.
5. Manually sign in via the GUI on signing server, so that the Keychains can be unlocked.  Then, switch to Rails directory, and run
```
VERBOSE=1 QUEUE=mac_online rake environment resque:work
```

### Configure Nginx
Use Let's Encrypt to obtain and automatically renew certificates.

```
# /etc/nginx/sites-enabled/appstore-http.conf

server {
  listen 80;
  server_name appstore.colab.duke.edu;

  include letsencrypt.conf;

  location / {
    rewrite     ^   https://$server_name$request_uri? redirect;
  }
}
```

```
# /etc/nginx/sites-enabled/appstore.conf

upstream puma {
  server unix:///home/appstore/appstore/shared/tmp/sockets/puma.sock;
}

server {
  listen 443 ssl;
  server_name appstore.colab.duke.edu;

  ssl_certificate /etc/letsencrypt/live/appstore.colab.duke.edu/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/appstore.colab.duke.edu/privkey.pem;

  client_max_body_size 500m;

  root /home/appstore/appstore/current/public;
  try_files $uri/index.html $uri @app;

  location /system/app_files/archives {
    internal;
  }

  location @app {
    proxy_pass http://puma;
    proxy_set_header Host $host;
    proxy_set_header X-Forwarded-Proto https;
    proxy_set_header X-Forwarded-For $remote_addr;
    proxy_set_header X-Sendfile-Type X-Accel-Redirect;
  }
}
```

### Set RAILS_ENV for scripts to work
In order for background jobs to run correctly without development gems like `sqlite3` (among other reasons), set the environment variable so the scripts are aware that it is in production mode.

```
# /home/appstore/.bash_profile
export RAILS_ENV=production

# (other RVM stuff)
```

### Auto-start Rails app on app server
#### Rails server
```
# /etc/systemd/system/appstore.service

[Unit]
Description=App Store Puma daemon
After=network.target

[Service]
WorkingDirectory=/home/appstore/appstore/current
ExecStart=/bin/bash -l -c 'bundle exec puma -C /home/appstore/appstore/shared/puma.rb'
User=appstore

[Install]
WantedBy=multi-user.target
```

#### Background jobs
```
# /etc/systemd/system/appstore_worker.service

[Unit]
Description=App Store background jobs worker
After=network.target

[Service]
WorkingDirectory=/home/appstore/appstore/current
ExecStart=/bin/bash -l -c 'bundle exec god -c config/god/web_worker.rb --no-daemonize'
User=appstore

[Install]
WantedBy=multi-user.target
```

### See Puma (app server) status
```
bundle exec pumactl -F /home/appstore/appstore/shared/puma.rb status
```

By the way, Puma socket is at:
```
unix:///home/appstore/appstore/shared/tmp/sockets/puma.sock
```

## TODO
* Better instructions about how to upload an app
* Also log download counts by application, so when we delete history versions, the counts will still be there
* Logrotate (seems like Logger has rotation capability)
* Only keep most recent 3 app versions
* Add ability for app owner to test the app, app version before marking them as 'Published'
* Better app ordering, categories, etc.
* Sidebar on app download page to showcase related apps, as well as more info about us
* Slack integration
* Support for Android apps
* External app store support